package id.pgd.demo.person;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class PersonService {

    public ResponseEntity<String> getPerson(BigDecimal bd) {
        if (!BigDecimal.ONE.equals(bd))
            return ResponseEntity.internalServerError().body("Data tidak sesuai");
        return ResponseEntity.ok().body("Berhasil Satu");
    }
}
