package id.pgd.demo.account;

import org.springframework.data.repository.ListCrudRepository;

import java.util.Optional;

public interface AccountRepo extends ListCrudRepository<AccountEntity, Integer> {

    Optional<AccountEntity> findByAccountIdAndPersonId(Integer accountId, Integer personId);
}
