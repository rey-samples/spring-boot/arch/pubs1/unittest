package id.pgd.demo.account;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.math.BigDecimal;

@JsonInclude(JsonInclude.Include.NON_NULL)
public record AccountDto(
        Integer accountId,
        Integer personId,
        @JsonInclude(JsonInclude.Include.NON_EMPTY) BigDecimal ammount)
{}
