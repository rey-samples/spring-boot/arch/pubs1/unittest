package id.pgd.demo.account;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AccountService {
    private final AccountRepo accountRepo;

    public AccountService(AccountRepo accountRepo) {
        this.accountRepo = accountRepo;
    }

    public ResponseEntity<Object> getAccount(Integer accountId, Integer personId) {
        if (accountId == null || personId == null)
            return ResponseEntity.badRequest().body("AccountID and PersonId must not be empty ");

//        return accountRepo.findByAccountIdAndPersonId(accountId, personId)
//                .map(acc -> ResponseEntity.ok(new AccountDto(acc.getAccountId(), acc.getPersonId(), acc.getAmount())))
//                .orElse(ResponseEntity.status(HttpStatus.NOT_FOUND).body("Account Not Found for account: " + accountId));

        Optional<AccountEntity> optionalAccountEntity = accountRepo.findByAccountIdAndPersonId(accountId, personId);
        if (optionalAccountEntity.isPresent()) {
            AccountEntity acc = optionalAccountEntity.get();
            return ResponseEntity.ok()
                    .body(new AccountDto(
                            acc.getAccountId(),
                            acc.getPersonId(),
                            acc.getAmount()));
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Account Not Found for account: " + accountId);
    }


}
